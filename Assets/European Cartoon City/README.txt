
These are assets for creating a European city or town.

All street pieces and houses are tileable.

This pack contains a total of 43 models.

This pack contains:

	- 1 Billboard		104 tris
	- 1 Fire Station	283 tris
	- 1 Gazebo		350 tris
	- 1 Hospital		354 tris
	- 3 Houses		82-166 tris
	- 1 Park		1644 tris
	- 1 Police Station	384 tris
	- 1 Snackbar		212 tris
	- 1 Taxi Station	435 tris
	- 1 Tree		160 tris
	- 15 Street Pieces	2-448 tris
	- 10 Traffic Signs	54-414 tris
	- 1 Car			196 tris
	- 1 Firetruck		1170 tris
	- 1 Icecream Truck	396 tris
	- 1 Police Car		240 tris
	- 1 Taxi		206 tris
	- 1 Truck		228 tris


The demo scene has a total polycount of approximately 136000 Tris.

The maximum Texture size is 1024x1024

.

Demo Level: http://www.lugus.be/CartoonCity/City.html
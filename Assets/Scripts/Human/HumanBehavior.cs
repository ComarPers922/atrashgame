﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanBehavior : MonoBehaviour
{
    [SerializeField]
    private bool Female = false;

    private Animator Animator;
    void Start()
    {
        Animator = GetComponent<Animator>();
        Animator.SetBool("Female", Female);
    }

    void Update()
    {
        transform.Translate(Vector3.forward * 20 * Time.deltaTime);
        Animator?.SetInteger("Speed", 1);
    }
}

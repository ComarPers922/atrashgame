﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class ExitGame : MonoBehaviour
{

#if UNITY_STANDALONE
    private Keyboard Keyboard;

    private void Awake()
    {
        Keyboard = InputSystem.GetDevice<Keyboard>();
    }

    void Update()
    {
        if (Keyboard.escapeKey.wasPressedThisFrame)
        {
            Application.Quit();
        }
    }
#endif
}

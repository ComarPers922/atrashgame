﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterPlatform : MonoBehaviour
{
    [SerializeField]
    private GameObject DesktopWater;
    [SerializeField]
    private GameObject MobileWater;
    void Start()
    {
#if UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL
        DesktopWater.SetActive(false);
        MobileWater.SetActive(true);
#elif UNITY_EDITOR
        // Do nothing...
#else
        DesktopWater.SetActive(true);
        MobileWater.SetActive(false);
#endif
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Permissions;
using UnityEngine;
using UnityEngine.UI;

public class TimePanel : MonoBehaviour
{
    [SerializeField]
    private Image ClockIcon;
    [SerializeField]
    private int GameTime = 120;

    private int CurrentTime;

    private GameManager GameManager;

    private Text TimeText;

    private AudioSource TimeAudio;

    public float TimeProgress 
    {
        get
        {
            return CurrentTime / (float)GameTime;
        }
    }

    void Start()
    {
        GameManager = Shared.GetMapManager().GetComponent<GameManager>();
        CurrentTime = GameTime;
        TimeText = GetComponent<Text>();
        TimeAudio = GetComponent<AudioSource>();
        if(GameManager.IsFirstTimeToPlay)
        {
            CurrentTime = 50;
        }
        Invoke(nameof(TikTok), 1);
    }

    public void BonusTime(int time)
    {
        CurrentTime += time;
    }

    public int GetCurrentTime()
    {
        return CurrentTime;
    }

    void Update()
    {
        switch(GameManager.PushdownStatus.Peek())
        {
            case GameSatus.InGame:
                TimeText.text = $"{Localization.GetLocalizedText("time")}: {CurrentTime}";
                break;
            case GameSatus.Gameover:
                TimeText.text = Localization.GetLocalizedText("timesup");
                break;
            case GameSatus.MainMenu:
                TimeText.text = Localization.GetLocalizedText("ready");
                break;
            case GameSatus.TutorialMenu:
                TimeText.text = Localization.GetLocalizedText("ready");
                break;
            case GameSatus.TutorialGame:
                if(!Shared.TutorialGameManager.IsCountingTime)
                {
                    TimeText.text = $"{Localization.GetLocalizedText("time")}: ∞";
                }
                else
                {
                    TimeText.text = $"{Localization.GetLocalizedText("time")}: {CurrentTime}";
                }
                break;
            case GameSatus.Pause:
                TimeText.text = $"{Localization.GetLocalizedText("gamepaused")}";
                break;
            default:
                TimeText.text = Localization.GetLocalizedText("error");
                break;
        }
        
    }

    private void TikTok()
    {
        ClockIcon.color = TimeText.color;
        if (GameManager.PushdownStatus.Peek() == GameSatus.InGame ||
            (GameManager.PushdownStatus.Peek() == GameSatus.TutorialGame &&
               Shared.TutorialGameManager.IsCountingTime))
        {
            if (CurrentTime-- <= 0)
            {
                var status = GameManager.PushdownStatus.Pop();
                if (status == GameSatus.InGame)
                {
                    PlayerPrefs.SetInt
                        (
                                Shared.HIGHEST_SCORE,

                                Math.Max
                                (
                                    GameManager.ScorePanel.Score,
                                    PlayerPrefs.GetInt(Shared.HIGHEST_SCORE, int.MinValue)
                                )
                        );
                    SocialManager.ReportGameScore(PlayerPrefs.GetInt(Shared.HIGHEST_SCORE, int.MinValue));
                }
            }
            if(CurrentTime <= 30 && CurrentTime > 15)
            {
                TimeText.color = new Color(235 / 255f, 193 / 255f, 84 / 255f);
            }
            else if(CurrentTime <= 15)
            {
                TimeText.color = Color.red;
            }
            else
            {
                TimeText.color = Color.black;
            }

            if(CurrentTime <= 5 && CurrentTime >= 0)
            {
                TimeAudio.clip = GameManager.LackOfTime;
                TimeAudio.Play();
#if !UNITY_ANDROID && !UNITY_IOS
                GameManager.VibrationManager.LongVibrate(.9f);
#else
                GameManager.VibrationManager.ShortVibrate(1);
#endif
            }
            switch (CurrentTime)
            {
                case 30:
                    TimeAudio.clip = GameManager.LackOfTime;
                    TimeAudio.Play();
                    GameManager.VibrationManager.LongVibrate(2);
                    break;
                case 15:
                    TimeAudio.clip = GameManager.LackOfTime;
                    TimeAudio.Play();
                    GameManager.VibrationManager.LongVibrate(2);
                    break;
                default:
                    break;
            }
        }
        Invoke(nameof(TikTok), 1);
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Shared
{
    private static GameManager Manager = null;
    private static TutorialGameManager TutorialManager = null;

    public static GameObject GetMapManager()
    {
        return GameObject.Find("MapManager");
    }

    public static GameManager GameManager
    {
        get
        {
            if(Manager == null)
            {
                Manager = GetMapManager().GetComponent<GameManager>();
            }
            return Manager;
        }
    }

    public static GameObject GetTutorialGameManager()
    {
        return GameObject.Find("TutorialGameManager");
    }

    public static TutorialGameManager TutorialGameManager
    {
        get
        {
            if(TutorialManager == null)
            {
                TutorialManager = GetTutorialGameManager().GetComponent<TutorialGameManager>();
            }
            return TutorialManager;
        }
    }

    public const string HIGHEST_SCORE = "HIGHEST_SCORE";
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowGamecenterUI : MonoBehaviour
{
    private Image GamecenterImage;
    private Button GameCenterButton;
    void Start()
    {
#if !UNITY_IOS && !UNITY_STANDALONE_OSX
    gameObject.SetActive(false);
#else
        GamecenterImage = GetComponent<Image>();
        GameCenterButton = GetComponent<Button>();
#endif
    }
    private void Update()
    {
        if (Shared.GameManager.PushdownStatus.Peek() == GameSatus.MainMenu ||
            Shared.GameManager.PushdownStatus.Peek() == GameSatus.Gameover ||
            Shared.GameManager.PushdownStatus.Peek() == GameSatus.TutorialMenu)
        {
            GameCenterButton.enabled = true;
            GamecenterImage.enabled = true;
        }
        else
        {
            GameCenterButton.enabled = false;
            GamecenterImage.enabled = false;
        }
    }
    public void ShowUI()
    {
        SocialManager.ShowLeaderboardUI();
    }
}

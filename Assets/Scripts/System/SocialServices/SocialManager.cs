﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_ANDROID
using GooglePlayGames;
using GooglePlayGames.BasicApi;
#endif

#if UNITY_IOS
using UnityEngine.SocialPlatforms;
using UnityEngine.SocialPlatforms.GameCenter;
#endif

public static class SocialManager
{
    private static bool IsSuccessful = false;

    public static void InitSocialServices()
    {
        if (IsSuccessful)
        {
            return;
        }
#if UNITY_ANDROID
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();
        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.DebugLogEnabled = true;

        PlayGamesPlatform.Activate();

        Social.Active.localUser.Authenticate(success=>
        {
            IsSuccessful = success;

            int score = PlayerPrefs.GetInt(Shared.HIGHEST_SCORE, int.MinValue);
            if (score != int.MinValue)
            {
                ReportGameScore(score);
            }
        });
#endif

#if UNITY_IOS || UNITY_STANDALONE_OSX
        Social.localUser.Authenticate((result) =>
        {
            IsSuccessful = result;

            int score = PlayerPrefs.GetInt(Shared.HIGHEST_SCORE, int.MinValue);
            if (score != int.MinValue)
            {
                ReportGameScore(score);
            }
        });
#endif
    }

    #region Achievements
    public static void UnlockCertifiedJanitor()
    {
        if(!IsSuccessful)
        {
            return;
        }
#if UNITY_ANDROID
        Social.ReportProgress(GooglePlayIds.achievement_certified_janitor, 100, succeed=>{ });
#endif

#if UNITY_IOS || UNITY_STANDALONE_OSX
        Social.ReportProgress(GameCenterIds.ACHIEVEMENT_CERTIFIED_JANITOR, 100, succeed=> { });
#endif
    }
    #endregion /Achievements

    #region Leaderboards
    public static void ReportGameScore(long gameScore)
    {
        if(!IsSuccessful)
        {
            return;
        }
#if UNITY_ANDROID
        Social.ReportScore(gameScore, GooglePlayIds.leaderboard_score, succeed => { });
#endif

#if UNITY_IOS || UNITY_STANDALONE_OSX
        Social.ReportScore(gameScore, GameCenterIds.LEADERBOARD_SCORE, succeed=> { });
#endif
    }

    public static void ShowLeaderboardUI()
    {
        if(!IsSuccessful)
        {
            return;
        }
#if UNITY_IOS || UNITY_STANDALONE_OSX
        Social.ShowLeaderboardUI();
#endif
    }
    #endregion /Leaderboards
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Spawn : MonoBehaviour
{
    private static int SeedIncrement = 0;
    private static float StartIncrement = 0;

    protected bool UseStartIncrement = false;

    [SerializeField]
    protected float MinSpawnTime = 10;
    [SerializeField]
    protected float MaxSpawnTime = 15;

    protected GameManager Manager;

    protected virtual void Awake()
    {
        Manager = Shared.GetMapManager().GetComponent<GameManager>();
        Random.InitState((int)(SeedIncrement + Time.time));
        SeedIncrement++;
    }

    protected virtual void Start()
    {
        if (UseStartIncrement)
        {
            Invoke(nameof(SpawnObject), StartIncrement);
            Random.InitState((int)(SeedIncrement + Time.time));
            SeedIncrement++;
            StartIncrement += Random.Range(7, 15);
            if (StartIncrement >= 50)
            {
                StartIncrement = 0;
            }
        }
        else
        {
            SpawnObject();
        }
    }

    protected virtual void SpawnObject()
    {
        Invoke(nameof(SpawnObject), Random.Range(MinSpawnTime, MaxSpawnTime));
    }
    
}

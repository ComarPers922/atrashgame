﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UICamera : MonoBehaviour
{
    private Camera Camera;
    private RenderTexture TempTexture;
    void Start()
    {
        TempTexture = RenderTexture.GetTemporary(1,1);
        Camera = GetComponent<Camera>();
    }

    void Update()
    {
        if(Camera.targetTexture == null)
        {
            Camera.targetTexture = TempTexture;
        }
    }

    ~UICamera()
    {
        RenderTexture.ReleaseTemporary(TempTexture);
    }
}

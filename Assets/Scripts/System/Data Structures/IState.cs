﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void Action();
public interface IState
{
    Action InitAction {get;}

    Action UpdateAction {get;}

    Action EndAction {get;}
}

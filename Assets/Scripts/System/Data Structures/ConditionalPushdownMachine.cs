﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Schema;
using UnityEngine;

public delegate void StateTransitedHandler(IStateMachine sender, IState fromState);
public class ConditionalPushdownMachine : IStateMachine
{
    private readonly Stack<PushdownState> PushdownStates = new Stack<PushdownState>();

    public event StateTransitedHandler StateTransited;

    private bool IsInit = false;
    public IState GetCurrentState()
    {
        if(PushdownStates.Count <= 0)
        {
            return null;
        }
        return PushdownStates.Peek();
    }

    /// <summary>
    /// NOT Unity Update!!!
    /// </summary>
    public void Update()
    {
        if(!IsInit)
        {
            if(PushdownStates.Count > 0)
            {
                PushdownStates.Peek().InitAction();
                IsInit = true;
            }
        }
        if(PushdownStates.Count <= 0)
        {
            return;
        }
        if(PushdownStates.Peek().StateCondition())
        {
            var state = PushdownStates.Pop();
            state.EndAction();
            StateTransited?.Invoke(this, state);
            if(PushdownStates.Count >= 1)
            {
                PushdownStates.Peek().InitAction();
                IsInit = true;
            }
        }
        if (PushdownStates.Count >= 1)
        {
            PushdownStates.Peek().UpdateAction();
        }
    }

    public void PushNewState(PushdownState newState, bool doInitAction = true)
    {
        IsInit = !doInitAction;
        PushdownStates.Push(newState);
    }

    public PushdownState ForceToPopState(bool doEndAction = true)
    {
        if(PushdownStates.Count <= 0)
        {
            return null;
        }
        var state = PushdownStates.Pop();
        if(doEndAction)
        {
            state.EndAction();
        }
        if(PushdownStates.Count > 1)
        {
            PushdownStates.Peek().InitAction();
        }
        return state;
    }
}

﻿public delegate bool TransitionCondition();

public class PushdownState : IState
{
    public TransitionCondition StateCondition { private set; get;}

    public Action InitAction { private set; get; }
    public Action UpdateAction { private set; get; }
    public Action EndAction { private set; get; }

    public PushdownState(TransitionCondition condition, Action init, Action update, Action end)
    {
        StateCondition = condition;
        InitAction = init;
        UpdateAction = update;
        EndAction = end;
    }
}

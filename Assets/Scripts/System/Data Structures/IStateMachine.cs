﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IStateMachine
{
    void Update();
    IState GetCurrentState();
}

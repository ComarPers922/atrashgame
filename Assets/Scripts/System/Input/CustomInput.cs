﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CustomInput
{
    private readonly static  Dictionary<string, float> Axis = new Dictionary<string, float>();

    public static void SetAxis(string axis, float value)
    {
        if(!Axis.ContainsKey(axis))
        {
            Axis.Add(axis, 0);
        }
        Axis[axis] = value;
    }

    public static float GetAxis(string axis)
    {
        if(!Axis.ContainsKey(axis))
        {
            return 0;
        }
        return Axis[axis];
    }

    public static void Clear()
    {
        Axis.Clear();
    }
}

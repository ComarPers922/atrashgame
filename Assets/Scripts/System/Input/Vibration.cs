﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.XR;
using Unity.XR.OpenVR;
using UnityEngine.InputSystem;

public class Vibration : MonoBehaviour
{
    public bool IsVibrating { private set; get; } = false;

    private int numOfVibrations = 0;

    private bool IsLongVibrating = false;
    public void LongVibrate(float duration)
    {
        IsLongVibrating = true;
        IsVibrating = true;
        Invoke(nameof(DoLongVibrate), 0.1f);
        Invoke(nameof(StopVibrating), duration);
    }

    public void ShortVibrate(int numOfVibrations)
    {
        if(IsVibrating)
        {
            return;
        }
        IsVibrating = true;
        this.numOfVibrations = numOfVibrations - 1;
        DoShortVibrate();
    }

    public void StopVibrating()
    {
        IsLongVibrating = false;
        IsVibrating = false;
        numOfVibrations = 0;
#if !UNITY_IOS && !UNITY_ANDROID
        Gamepad.current?.SetMotorSpeeds(0, 0);
#endif
    }
#if !UNITY_IOS && !UNITY_ANDROID
    private bool IsShaking = false;
#endif
    private void DoShortVibrate()
    {
        if(IsLongVibrating)
        {
            return;
        }

#if UNITY_IOS || UNITY_ANDROID
        Handheld.Vibrate();
#else
        if(!IsShaking)
        {
            Gamepad.current?.SetMotorSpeeds(.7f, .7f);
            IsShaking = true;
            Invoke(nameof(DoShortVibrate), 0.25f);
            return;
        }
        Gamepad.current?.SetMotorSpeeds(0, 0);
        IsShaking = false;
#endif
        if (numOfVibrations -- > 0)
        {
            Invoke(nameof(DoShortVibrate), 0.5f);
        }
        else
        {
            IsVibrating = false;
        }
    }

    private void DoLongVibrate()
    {
        if(IsVibrating)
        {
#if UNITY_IOS || UNITY_ANDROID
            Handheld.Vibrate();
            Invoke(nameof(DoLongVibrate), 0.1f);
#else
            Gamepad.current?.SetMotorSpeeds(0.9f, 0.9f);
#endif
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEngine;
using UnityEngine.InputSystem;

public class SetupInput : MonoBehaviour
{
#if (!UNITY_IOS && !UNITY_ANDROID) || UNITY_EDITOR
    private GameControl GameControl;
    private void Awake()
    {
        GameControl = new GameControl();

        #region Collect & Release
        GameControl.Player.Collect.performed += (context) =>
        {
            CustomInput.SetAxis("Collect", context.ReadValue<float>());
        };

        GameControl.Player.Collect.canceled += (context) =>
        {
            CustomInput.SetAxis("Collect", 0);
        };

        GameControl.Player.Release.performed += (context) =>
        {
            CustomInput.SetAxis("Release", context.ReadValue<float>());
        };

        GameControl.Player.Release.canceled += (context) =>
        {
            CustomInput.SetAxis("Release", 0);
        };
        #endregion

        #region Regular Movement
        GameControl.Player.Movement.performed += (context) =>
        {
            Vector2 axis = context.ReadValue<Vector2>();
            CustomInput.SetAxis("Horizontal", axis.x);
            CustomInput.SetAxis("Vertical", axis.y);
        };
        GameControl.Player.Movement.canceled += (context) =>
        {
            CustomInput.SetAxis("Horizontal", 0);
            CustomInput.SetAxis("Vertical", 0);
        };
        #endregion

        #region Accelerate
        GameControl.Player.Accelerate.performed += (context) =>
        {
            CustomInput.SetAxis("Sprint", context.ReadValue<float>());
        };

        GameControl.Player.Accelerate.canceled += (context) =>
        {
            CustomInput.SetAxis("Sprint", 0);
        };
        #endregion

        #region Pause
        GameControl.Player.Pause.performed += (context) =>
        {
            if(Shared.GameManager.PushdownStatus.Peek() == GameSatus.InGame)
            {
                Shared.GameManager.PauseGame();
            }
        };
        #endregion
    }

    private void OnEnable()
    {
        GameControl.Enable();
    }

    private void OnDisable()
    {
        foreach (var item in Gamepad.all)
        {
            item.SetMotorSpeeds(0, 0);
        }
        GameControl.Disable();
    }
    #endif
}

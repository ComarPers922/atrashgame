﻿#define TutorialTesterBuild
#undef TutorialTesterBuild

using System;
using System.Collections.Generic;
using System.Collections;
using System.IO;
using System.Xml;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;


public enum GameSatus
{
    Unknown, MainMenu, InGame, TutorialMenu, TutorialGame, Pause, Gameover
}

public class GameManager : MonoBehaviour
{
    public GameObject[] Cars;
    public GameObject[] Birds;
    public GameObject[] Airplanes;
    public GameObject[] Trashes;
    public GameObject[] Powerups;
    public TrashViewer TrashViewer;
    public ScorePanel ScorePanel;
    public readonly Queue<RenderTexture> TextureReleasePool = new Queue<RenderTexture>();
    public readonly Stack<GameSatus> PushdownStatus = new Stack<GameSatus>();
    public Vibration VibrationManager;
    public bool IsFirstTimeToPlay = true;
    public Text TutorialText;
    public TutorialWallBehavior TutorialWall;
    public TimePanel GameTime;

    public AudioClip VacuumRunning;
    public AudioClip VacuumShutdown;
    public AudioClip TrashReleased;
    public AudioClip TrashProcessing;
    public AudioClip TrashCorrect;
    public AudioClip TrashWrong;
    public AudioClip Powerup;
    public AudioClip HazardousTrash;
    public AudioClip HazardousTrashEmerged;
    public AudioClip LackOfTime;
    public AudioClip GameOverSound;
    public AudioClip GameStartSound;
    public AudioClip TrashFled;
    public AudioClip PauseSound;
    public AudioClip SpeedUpSound;

    public bool IsTrashPaused{ private set; get; }

    [SerializeField]
    private Camera UICamera;

    private AudioSource ManagerAudio;

    public int NumCars { set; get; } = 15;
    public int NumBirds { set; get; } = 15;
    public int NumAirplanes { set; get; } = 5;
    public int NumTrashes { set; get; } = 10;
    public int NumPowerups { set; get; } = 3;

#if (UNITY_ANDROID || UNITY_WEBGL) && !UNITY_EDITOR
    private IEnumerator InitLocalizationByWebRequest()
    {
        var path = Application.streamingAssetsPath;
        var localizationFile = Path.Combine(path, "localization.xml");
        var uri = new Uri(localizationFile);

        UnityWebRequest request = UnityWebRequest.Get(uri);
        DownloadHandlerBuffer buffer = new DownloadHandlerBuffer();
        request.downloadHandler = buffer;
        yield return request.SendWebRequest();
 
        MemoryStream xmlStream = new MemoryStream(request.downloadHandler.data);
        WriteToDictionary(new StreamReader(xmlStream));

        TutorialText.text = $"{Localization.GetLocalizedText("welcome")}\n{(IsFirstTimeToPlay ? $"({Localization.GetLocalizedText("tutorial")})" : "")}";
    }

#else
    private void InitLocalizationByFileIO()
    {
        var path = Application.streamingAssetsPath;
        var localizationFile = Path.Combine(path, "localization.xml");

        using (StreamReader stream = new StreamReader(localizationFile))
        {
            WriteToDictionary(stream);
        }
        TutorialText.text = $"{Localization.GetLocalizedText("welcome")}\n{(IsFirstTimeToPlay ? $"({Localization.GetLocalizedText("tutorial")})" : "")}";
    }
#endif

    private void WriteToDictionary(StreamReader stream)
    {
        using (stream)
        {
            XmlDocument xml = new XmlDocument();

            XmlReader reader = XmlReader.Create(stream, new XmlReaderSettings()
            {
                IgnoreComments = true
            });

            xml.Load(reader);
            var text = xml.GetElementsByTagName("content")[0];
            foreach (XmlNode node in text)
            {
                var id = node.Attributes["id"].Value;
                var nodes = node.ChildNodes;
                foreach (XmlNode item in nodes)
                {
                    Localization.AddNewTextPair(id,
                            Localization.StringToLanguageType(item.Name),
                            item.Attributes["text"].Value);
                }
            }
        }
    }

    private void Awake()
    {
#if UNITY_ANDROID || UNITY_IOS
        try
        {
            SocialManager.InitSocialServices();
        }
        catch
        {
            // Prevent game from totally crashing...
        }
#endif
        ManagerAudio = GetComponent<AudioSource>();
#if TutorialTesterBuild
        PlayerPrefs.DeleteKey(nameof(IsFirstTimeToPlay));
#endif
        CustomInput.Clear();
        IsFirstTimeToPlay = PlayerPrefs.GetInt(nameof(IsFirstTimeToPlay), 1) == 1;
#if (UNITY_WEBGL || UNITY_ANDROID) && !UNITY_EDITOR
        StartCoroutine(InitLocalizationByWebRequest());
#else
        InitLocalizationByFileIO();
#endif
        PushdownStatus.Push(GameSatus.Unknown);

        if(IsFirstTimeToPlay)
        {
            PushdownStatus.Push(GameSatus.TutorialMenu);
        }
        else
        {
            SocialManager.UnlockCertifiedJanitor();
            PushdownStatus.Push(GameSatus.MainMenu);
            Shared.TutorialGameManager.gameObject.SetActive(false);
            TutorialWall.gameObject.SetActive(false);
        }
        Application.targetFrameRate = 300;
    }

    private void Update()
    {
        if(!Application.isFocused && PushdownStatus.Peek() == GameSatus.InGame)
        {
            PauseGame();
        }
        while (TextureReleasePool.Count > 0)
        {
            UICamera.targetTexture = null;
            var texture = TextureReleasePool.Dequeue();
            RenderTexture.ReleaseTemporary(texture);
        }
        if (Shared.GameManager.PushdownStatus.Peek() == GameSatus.Pause)
        {
            if (CustomInput.GetAxis("Collect") > 0.1f ||
            CustomInput.GetAxis("CollectMobile") > 0.1f)
            {
                Shared.GameManager.ResumeGame();
            }
        }
    }

    public void PauseTrash(float time)
    {
        IsTrashPaused = true;
        CancelInvoke(nameof(ReleaseTrash));
        Invoke(nameof(ReleaseTrash), time);
    }

    private void ReleaseTrash()
    {
        IsTrashPaused = false;
    }

    private void PlayPauseSound()
    {
        ManagerAudio.clip = PauseSound;
        ManagerAudio.Play();
    }

    public void PauseGame()
    {
        if (PushdownStatus.Peek() != GameSatus.Pause)
        {
            PlayPauseSound();
            PushdownStatus.Push(GameSatus.Pause);
            Time.timeScale = 0;
        }
    }

    public void ResumeGame()
    {
        if (PushdownStatus.Peek() == GameSatus.Pause)
        {
            PlayPauseSound();
            PushdownStatus.Pop();
        }
        Time.timeScale = 1;
    }

    private void OnApplicationPause(bool pause)
    {
        if(pause && PushdownStatus.Peek() == GameSatus.InGame)
        {
            PauseGame();
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public struct TrashGroup
{
    public TrashCanBehavior TrashCan;
    public Highlight TrashMountain;
    public Image TrashTipText;
}

public class TutorialGameManager : MonoBehaviour
{
    public GameObject[] PaperTrashes;

    [SerializeField]
    private Text TutorialText;
    [SerializeField]
    private GameObject ButtonA;
    [SerializeField]
    private GameObject ButtonB;
    [SerializeField]
    private GameObject JoyStick;
    [SerializeField]
    private GameObject[] TrashSpawnPoints;
    [SerializeField, Tooltip("Put paper trashcan at the index 0.")]
    private TrashGroup[] TrashCans;
    [SerializeField]
    private GameObject Player;
    [SerializeField]
    private GameObject TutorialInvisibleWall;

    private int NumTrashes;

    private float PlayerMovementDistance = 0;
    private Vector2 PreviousPlayerPosition;

    public bool IsCountingTime { private set; get; } = false;
    public bool IsReleasingTrash { private set; get; } = false;

    private readonly ConditionalPushdownMachine ConditionalPushdownMachine = new ConditionalPushdownMachine();
    void Start()
    {
        PreviousPlayerPosition = new Vector2(Player.transform.position.x, Player.transform.position.z);
        NumTrashes = Shared.GameManager.NumTrashes;
        Shared.GameManager.NumTrashes = 2;
#if UNITY_IOS || UNITY_ANDROID
        ButtonB.SetActive(false);
        JoyStick.SetActive(false);
#endif
        foreach (var item in TrashSpawnPoints)
        {
            item.SetActive(false);
        }
        foreach (var item in TrashCans)
        {
            item.TrashCan.gameObject.SetActive(false);
            item.TrashMountain.gameObject.SetActive(false);
            item.TrashTipText.gameObject.SetActive(false);
        }
        InitTutorial();
    }

    private void InitTutorial()
    {
        var initState = new PushdownState(
            delegate ()
            {
                return PlayerMovementDistance >= 200;
            },
            delegate ()
            {
#if UNITY_ANDROID || UNITY_IOS
                TutorialText.text = Localization.GetLocalizedText("tutorialMovementMobile");
                CustomInput.Clear();
                ButtonA.SetActive(false);
                JoyStick.SetActive(true);
#else
                TutorialText.text = Localization.GetLocalizedText("tutorialMovement");
#endif
            },
            delegate ()
            {
                var currentPosition = new Vector2(Player.transform.position.x, Player.transform.position.z);
                var MoveDistance = Vector2.Distance(currentPosition, PreviousPlayerPosition);
                if(MoveDistance <= 0.2f)
                {
                    MoveDistance = 0;
                }
                PlayerMovementDistance += MoveDistance;
                PreviousPlayerPosition = currentPosition;
            },
            delegate ()
            {
                Shared.GameManager.ScorePanel.PushStringStatus("Nice!");
                Shared.GameManager.VibrationManager.ShortVibrate(2);
            });

        var collectState = new PushdownState(
            delegate ()
            {
                return Shared.GameManager.TrashViewer.Count >= 2;
            },
            delegate ()
            {
#if UNITY_IOS || UNITY_ANDROID
                ButtonA.SetActive(true);
#endif
                IsReleasingTrash = true;
                TrashSpawnPoints[UnityEngine.Random.Range(0, 2)].SetActive(true);
            },
            delegate (){
#if UNITY_ANDROID || UNITY_IOS
            TutorialText.text = $"{Localization.GetLocalizedText($"tutorialCollectionMobile")}: ({Shared.GameManager.TrashViewer.Count}/2)";
#else
            TutorialText.text =  $"{Localization.GetLocalizedText($"tutorialCollection")}: ({Shared.GameManager.TrashViewer.Count}/2)";
#endif
            },
            delegate ()
            {
                Shared.GameManager.ScorePanel.PushStringStatus("Nice!");
                Shared.GameManager.VibrationManager.ShortVibrate(2);
            }
            );

        var throwState = new PushdownState(
            delegate()
            {
                return Shared.GameManager.TrashViewer.Count <= 0;
            },
            delegate()
            {
#if UNITY_IOS || UNITY_ANDROID
                ButtonB.SetActive(true);
#endif
                TrashCans[0].TrashCan.gameObject.SetActive(true);
                TrashCans[0].TrashMountain.gameObject.SetActive(true);
                TrashCans[0].TrashTipText.gameObject.SetActive(true);
            },
            delegate()
            {
#if UNITY_ANDROID || UNITY_IOS
            TutorialText.text = $"{Localization.GetLocalizedText($"tutorialThrowMobile")}: ({2 - Shared.GameManager.TrashViewer.Count}/2)";
#else
            TutorialText.text =  $"{Localization.GetLocalizedText($"tutorialThrow")}: ({2 - Shared.GameManager.TrashViewer.Count}/2)";
#endif
            },
            delegate()
            {
                Shared.GameManager.ScorePanel.PushStringStatus("Nice!");
                Shared.GameManager.VibrationManager.ShortVibrate(2);
            }
            );

        var tutorialState = new PushdownState(
            delegate()
            {
                return Shared.GameManager.PushdownStatus.Peek() == GameSatus.Gameover;
            },
            delegate()
            {
                TutorialInvisibleWall.SetActive(false);
                IsCountingTime = true;
                Shared.GameManager.NumTrashes = NumTrashes;
                foreach (var item in TrashCans)
                {
                    item.TrashCan.gameObject.SetActive(true);
                    item.TrashMountain.gameObject.SetActive(true);
                    item.TrashTipText.gameObject.SetActive(true);
                }
                foreach (var item in TrashSpawnPoints)
                {
                    item.SetActive(true);
                }
                TutorialText.text = Localization.GetLocalizedText("tutorialGame");
            },
            delegate()
            {

            },
            delegate()
            {
                TutorialText.text = Localization.GetLocalizedText("tutorialEnd");
                Shared.GameManager.ScorePanel.PushStringStatus("Nice!");
                PlayerPrefs.SetInt("IsFirstTimeToPlay", -1);
                SocialManager.UnlockCertifiedJanitor();
            }
            );
        ConditionalPushdownMachine.PushNewState(tutorialState,false);
        ConditionalPushdownMachine.PushNewState(throwState,false);
        ConditionalPushdownMachine.PushNewState(collectState,false);
        ConditionalPushdownMachine.PushNewState(initState,true);
    }
   
    void Update()
    {
        if (Shared.GameManager.PushdownStatus.Peek() == GameSatus.TutorialGame ||
            Shared.GameManager.PushdownStatus.Peek() == GameSatus.Gameover)
        {
            ConditionalPushdownMachine.Update();
        }
    }
}

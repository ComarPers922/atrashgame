﻿using UnityEngine;

public class TutorialWallBehavior : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Trash"))
        {
            other.GetComponent<TrashBehavior>()?.Block();
        }
    }
}

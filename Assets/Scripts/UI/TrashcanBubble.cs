﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TrashcanBubble : MonoBehaviour
{
    private Image BubbleImage;
    private Text BubbleText;
    void Start()
    {
        BubbleImage = GetComponent<Image>();
        BubbleText = GetComponentInChildren<Text>();

        HideUI();
    }

    private void HideUI()
    {
        BubbleImage.enabled = false;
        BubbleText.enabled = false;
    }
    private void ShowUI()
    {
        BubbleImage.enabled = true;
        BubbleText.enabled = true;
    }


    public void ShowConversation(string content)
    {
        CancelInvoke();

        BubbleText.text = content;

        ShowUI();

        Invoke(nameof(HideUI), 2f);
    }    
}

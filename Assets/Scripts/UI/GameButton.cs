﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameButton : MonoBehaviour
{
    [SerializeField]
    private Text ButtonText;

    private Image ImageBackground;

    private GameManager GameManager;

    private AudioSource GameButtonAudio;

    void Start()
    {
        GameButtonAudio = GetComponent<AudioSource>();
        GameManager = Shared.GameManager;
        ImageBackground = GetComponent<Image>();
    }

    void Update()
    {
        switch (GameManager.PushdownStatus.Peek())
        {
            case GameSatus.MainMenu:
#if UNITY_STANDALONE
                ButtonText.text = Localization.GetLocalizedText("start");
#else
                ButtonText.text = Localization.GetLocalizedText("startmobile");
#endif
                if (CustomInput.GetAxis("Collect") > 0.1f || CustomInput.GetAxis("CollectMobile") > 0.1f)
                {
                    GameButtonAudio.clip = GameManager.GameStartSound;
                    GameButtonAudio.Play();
                    GameManager.VibrationManager.LongVibrate(1);
                    ButtonText.gameObject.SetActive(false);
                    ImageBackground.enabled = false;
                    GameManager.PushdownStatus.Push(GameSatus.Gameover);
                    GameManager.PushdownStatus.Push(GameSatus.InGame);
                    GameManager.TutorialText.transform.parent.gameObject.SetActive(false);
                }
                break;
            case GameSatus.InGame:
                HidePause();
                break;
            case GameSatus.Pause:
                ShowPause();
                break;
            case GameSatus.Gameover:
                if (!IsShowingRestart)
                {
                    GameButtonAudio.clip = GameManager.GameOverSound;
                    GameButtonAudio.Play();
                    GameManager.VibrationManager.LongVibrate(2);
                    Invoke(nameof(ShowRestart), 3f);
                    IsShowingRestart = true;
                } 
                if(IsReadyToRestart)
                {
#if UNITY_STANDALONE
                            ButtonText.text = Localization.GetLocalizedText("restart");
#else
                            ButtonText.text = Localization.GetLocalizedText("restartmobile");;
#endif
                    if (CustomInput.GetAxis("Collect") > 0.1f || CustomInput.GetAxis("CollectMobile") > 0.1f)
                    {
                        ButtonText.text = Localization.GetLocalizedText("loading");
                        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                    }
                }
                break;
            case GameSatus.TutorialMenu:
#if UNITY_STANDALONE
                ButtonText.text = Localization.GetLocalizedText("start");
#else
                ButtonText.text = Localization.GetLocalizedText("startmobile");
#endif
                if (CustomInput.GetAxis("Collect") > 0.1f || CustomInput.GetAxis("CollectMobile") > 0.1f)
                {
                    ButtonText.gameObject.SetActive(false);
                    ImageBackground.enabled = false;
                    GameManager.PushdownStatus.Push(GameSatus.Gameover);
                    GameManager.PushdownStatus.Push(GameSatus.TutorialGame);
                }
                break;
            default:
                break;
        }
    }
    private bool IsShowingRestart = false;
    private bool IsReadyToRestart = false;
    private void ShowRestart()
    {
        ButtonText.gameObject.SetActive(true);
        ImageBackground.enabled = true;
        IsReadyToRestart = true;
    }

    private void ShowPause()
    {
#if !UNITY_STANDALONE && !UNITY_WEBGL
        ButtonText.text = Localization.GetLocalizedText("howtoresumeA");
#else
        ButtonText.text = Localization.GetLocalizedText("howtoresumeSpace");
#endif
        ButtonText.gameObject.SetActive(true);
        ImageBackground.enabled = true;
        IsReadyToRestart = true;
    }


    private void HidePause()
    {
        ButtonText.gameObject.SetActive(false);
        ImageBackground.enabled = false;
        IsReadyToRestart = false;
    }
}

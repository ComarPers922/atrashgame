﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseButton : MonoBehaviour
{
    private Image ButtonImage;
    private Button Button;

    private void Start()
    {
#if !UNITY_IOS && !UNITY_ANDROID && !UNITY_EDITOR
        gameObject.SetActive(false);
        return;
#endif
        ButtonImage = GetComponent<Image>();
        Button = GetComponent<Button>();
    }

    private void Update()
    {
        var isInGame = (Shared.GameManager.PushdownStatus.Peek() == GameSatus.InGame);
        ButtonImage.enabled = isInGame;
        Button.enabled = isInGame;
    }

    public void PauseGame()
    {
        Shared.GameManager.PauseGame();
    }
}

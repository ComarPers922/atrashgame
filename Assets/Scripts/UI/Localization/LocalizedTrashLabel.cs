﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LocalizedTrashLabel : MonoBehaviour
{
    [SerializeField]
    private TrashType TrashType;
    void Start()
    {
        var resultText = TranslateTrashType(TrashType);

        if(resultText == "NULL")
        {
            Invoke(nameof(Start), 1);
        }
        GetComponent<Text>().text = resultText;
    }

    public static string TranslateTrashType(TrashType type)
    {
        var resultText = "Unknown";
        switch (type)
        {
            case TrashType.Plastic:
                resultText = Localization.GetLocalizedText("plastic");
                break;
            case TrashType.Glass:
                resultText = Localization.GetLocalizedText("glass");
                break;
            case TrashType.Paper:
                resultText = Localization.GetLocalizedText("paper");
                break;
            case TrashType.Metal:
                resultText = Localization.GetLocalizedText("metal");
                break;
            case TrashType.Hazardous:
                resultText = Localization.GetLocalizedText("hazardous");
                break;
            case TrashType.General:
                resultText = Localization.GetLocalizedText("general");
                break;
            default:
                break;
        }
        return resultText;
    }
}

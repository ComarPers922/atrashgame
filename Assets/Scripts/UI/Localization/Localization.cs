﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Xml.Serialization;
using UnityEngine;

public enum LanguageType
{
    en, cn, cnt
}

public static class Localization
{
    public static LanguageType StringToLanguageType(string text)
    {
        return (LanguageType)Enum.Parse(typeof(LanguageType), text);
    }

    public static readonly Dictionary<string, Dictionary<LanguageType, string>> LocalizationDictionary = new Dictionary<string, Dictionary<LanguageType, string>>();
    public static void AddNewTextPair(string id, LanguageType type, string text)
    {
        if(!LocalizationDictionary.ContainsKey(id))
        {
            LocalizationDictionary.Add(id, new Dictionary<LanguageType, string>());
        }
        if(!LocalizationDictionary[id].ContainsKey(type))
        {
            LocalizationDictionary[id].Add(type, text);
        }
        else
        {
            LocalizationDictionary[id][type] = text;
        }
    }

    public static string GetLocalizedText(string id)
    {
        if(!LocalizationDictionary.ContainsKey(id))
        {
            return "NULL";
        }
        switch (Application.systemLanguage)
        {
            case SystemLanguage.Chinese:
                return LocalizationDictionary[id][LanguageType.cn];

            case SystemLanguage.ChineseSimplified:
                return LocalizationDictionary[id][LanguageType.cn];

            case SystemLanguage.ChineseTraditional:
                return LocalizationDictionary[id][LanguageType.cnt];

            default:
                return LocalizationDictionary[id][LanguageType.en];
        }
    }
}

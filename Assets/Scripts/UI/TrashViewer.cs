﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine.UI;

public class TrashViewer : MonoBehaviour
{
    private const float OFFSET_X = 95;

    [SerializeField]
    private TrashViewerCell CellPrefab;

    [SerializeField]
    private Camera TrashCamera;

    [SerializeField]
    private GameObject TrashCapturePoint;

    [SerializeField]
    private int MaxCapacity = 5;

    [SerializeField]
    private Text CapacityLabel;

    [SerializeField]
    private Color CurrentItemColor = Color.green;

    private readonly Queue<TrashViewerCell> TrashCells = new Queue<TrashViewerCell>();

    private GameManager GameManager;

    public int Count
    {
        get
        {
            return TrashCells.Count;
        }
    }

    public int Capacity
    {
        get
        {
            return MaxCapacity;
        }
    }

    public bool Enqueue(TrashBehavior newTrash)
    {
        if(TrashCells.Count >= MaxCapacity)
        {
            return false;
        }
        newTrash.gameObject.SetActive(false);
        var newCell = Instantiate(CellPrefab);
        newCell.transform.SetParent(transform);

        newCell.GetComponent<RectTransform>().localPosition = new Vector3(OFFSET_X * TrashCells.Count, 200, 0);
        newCell.InitMesh(newTrash, TrashCamera, TrashCapturePoint);

        TrashCells.Enqueue(newCell);
        return true;
    }

    public TrashBehavior Dequeue()
    {
        if(TrashCells.Count <= 0)
        {
            return null;
        }
        var front = TrashCells.Dequeue();
        var trash = front.TrashItem;
        front.Dispose();
        return trash;
    }
    private void Start()
    {
        GameManager = Shared.GameManager;
    }
    private void Update()
    {
        CapacityLabel.text = $"{Localization.GetLocalizedText("capacity")}: {TrashCells.Count}/{MaxCapacity}";
        if (GameManager.PushdownStatus.Peek() == GameSatus.Gameover)
        {
            while(TrashCells.Count > 0)
            {
                Dequeue();
            }
            return;
        }
        if (TrashCells.Count > 0)
        {
            var color = TrashCells.Peek().GetComponent<Image>().color;
            TrashCells.Peek().GetComponent<Image>().color = Color.Lerp(color, CurrentItemColor, 0.1f);
            TrashCells.Peek().transform.localScale = Vector3.Lerp(Vector3.one, Vector3.one * 1.5f, 0.1f);
        }
        int index = 0;
        foreach(var item in TrashCells)
        {
            var pos = item.GetComponent<RectTransform>().localPosition;
            item.GetComponent<RectTransform>().localPosition = Vector3.Lerp(pos, new Vector3(OFFSET_X * index++, 0, 0), Random.Range(0.05f, 0.1f));
        }
    }
}

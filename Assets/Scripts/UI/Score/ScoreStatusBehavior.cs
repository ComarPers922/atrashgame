﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreStatusBehavior : MonoBehaviour
{
    private RectTransform RectTransform;
    void Start()
    {
        RectTransform = GetComponent<RectTransform>();
        Invoke(nameof(Dispose), 5f);
    }

    private void Dispose()
    {
        Destroy(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        RectTransform.localPosition += Vector3.up * 50 * Time.deltaTime;
    }
}

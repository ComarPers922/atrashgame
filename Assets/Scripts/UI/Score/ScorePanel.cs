﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScorePanel : MonoBehaviour
{
    [SerializeField]
    private ScoreStatus ScoreStatus;
    [SerializeField]
    private Text ComboText;
    [SerializeField]
    private Image ComboProgressBar;
    [SerializeField]
    private Image ComboBackground;

    [Space(2)]
    [SerializeField]
    private Color ProgressBarGreenColor;
    [SerializeField]
    private Color ProgressBarRedColor;

    private Text ScoreText;

    private int Combo = 0;
    private float ComboProgress = 0;
    private int ScoreMultiplier = 1;

    private readonly Queue<KeyValuePair<string, Color>> StatusQueue = new Queue<KeyValuePair<string, Color>>();

    public int Score
    {
        private set
        {
            if(Shared.GameManager.PushdownStatus.Peek() == GameSatus.Gameover)
            {
                return;
            }
            int scoreChange = value - score;
            StatusQueue.Enqueue(new KeyValuePair<string, Color>($"{(scoreChange >= 0 ? "+":"")}{scoreChange}", (scoreChange < 0 ? Color.red : Color.green)));
            score = value;
        }
        get
        {
            return score;
        }
    }

    public int BadCount { private set; get; } = 0;

    public int GoodCount { private set; get; } = 0;

    public int MissCount { private set; get; } = 0;

    private GameManager GameManager;
    private int score = 0;
    private int highestscore = 0;

    private bool ShowHighestScore = false;
    private bool IsGameover = false;

    void Start()
    {
        ScoreText = GetComponent<Text>();
        GameManager = Shared.GetMapManager().GetComponent<GameManager>();
        CustomFixedUpdate();
        ComboTikTok();
    }
    private void CustomFixedUpdate()
    {
        if (StatusQueue.Count > 0)
        {
            var item = StatusQueue.Dequeue();
            ScoreStatus.NewStatus(item.Key, item.Value);
        }
        Invoke(nameof(CustomFixedUpdate), 0.5f);
    }
    
    void Update()
    {
        if(GameManager.PushdownStatus.Peek() == GameSatus.Gameover)
        {
            CancelCombo();
        }
        ScoreText.color = Color.black;
        ScoreText.text = $@"{(GameManager.PushdownStatus.Peek() == GameSatus.Gameover ? $"{Localization.GetLocalizedText("final")}" : "")}{Localization.GetLocalizedText("score")}: {Score}";
        if(!IsGameover && GameManager.PushdownStatus.Peek() == GameSatus.Gameover)
        {
            IsGameover = true;
            highestscore = Math.Max(score, PlayerPrefs.GetInt(Shared.HIGHEST_SCORE, int.MinValue));
            InvokeRepeating(nameof(SwitchScore), 3, 3);
        }
        if(GameManager.PushdownStatus.Peek() == GameSatus.Gameover && ShowHighestScore)
        {
            ScoreText.color = Color.red;
            ScoreText.text = $"{Localization.GetLocalizedText("highestscore")}: {highestscore}";
        }
        ComboText.text = $"{Localization.GetLocalizedText("combo")}: {Combo} (*{ScoreMultiplier})";
        ComboProgressBar.color = Color.Lerp(ProgressBarGreenColor, ProgressBarRedColor, 1- ComboProgress);
        ComboProgressBar.transform.localPosition = new Vector3(Mathf.Lerp(0, -220, 1 - ComboProgress),0,0);
    }
    private void SwitchScore()
    {
        ShowHighestScore = !ShowHighestScore;
    }
    public void AddScore()
    {
        Score += ((10 + (GoodCount / 5) * 5) * Mathf.Max(Combo ++, 1)) * 
                    Mathf.Max(ScoreMultiplier, 1);
        ComboProgress = 1;
        CancelInvoke(nameof(ComboTikTok));
        Invoke(nameof(ComboTikTok), 1);
    }

    public void MinusScoreWrongCollection()
    {
        Score -= (10 + (BadCount / 3) * 5);
        CancelCombo();
    }

    public void MinusScoreMiss()
    {
        Score -= (15 + (MissCount / 3) * 10);
        CancelCombo();
    }

    public void MinusScoreHazardous()
    {
        Score -= 10;
    }

    public void AddGoodCount()
    {
        GoodCount++;
    }

    public void AddBadCount()
    {
        BadCount++;
    }

    public void AddMissCount()
    {
        MissCount++;
    }

    private void ComboTikTok()
    {
        ComboProgress -= 0.1f;
        ComboProgress = Mathf.Max(ComboProgress, 0);
        if(ComboProgress <= 0)
        {
            Combo = 0;
        }
        Invoke(nameof(ComboTikTok), 1);
    }

    private void CancelCombo()
    {
        CancelInvoke(nameof(ComboTikTok));
        ComboProgress = 0;
        Combo = 0;
    }

    public void PushStringStatus(string content)
    {
        StatusQueue.Enqueue(new KeyValuePair<string, Color>(content, Color.green));
    } 
    
    public void SetScoreMultiplier(int multiplier)
    {
        ScoreMultiplier = multiplier;
        CancelInvoke(nameof(ResetMultiplier));
        Invoke(nameof(ResetMultiplier), 15);
    }

    private void ResetMultiplier()
    {
        ScoreMultiplier = 1;
    }
}

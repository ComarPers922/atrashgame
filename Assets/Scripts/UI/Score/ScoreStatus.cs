﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreStatus : MonoBehaviour
{
    [SerializeField]
    private Text StatusTextTemplate;

    public void NewStatus(string content, Color fontColor)
    {
        var newStatus = Instantiate<Text>(StatusTextTemplate);
        newStatus.transform.SetParent(transform);
        newStatus.rectTransform.localPosition = new Vector2(0, -50);
        newStatus.text = content;
        newStatus.color = fontColor;
    }
}

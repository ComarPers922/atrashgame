﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions.Must;
using UnityEngine.UI;

public class TrashViewerCell : MonoBehaviour, IDisposable
{
    [SerializeField]
    private RawImage ImageView;

    private static readonly Vector3 DisposalPoint = new Vector3(-1000, 0, 0);

    public TrashBehavior TrashItem { private set; get; }

    private Camera TrashCamera;

    private GameObject TrashCapturePoint;

    private Quaternion Rotation = Quaternion.Euler(-90, 0, 0);

    private RenderTexture ImageTexture;

    private Vector3 Scale = Vector3.one;

    private Mesh TrashMesh = null;

    private Material TrashMaterial = null;

    private bool IsInitialized = false;

    private bool IsDisposed = false;

    public void InitMesh(TrashBehavior item, Camera trashCamera, GameObject trashPoint)
    {
        TrashMesh = item.gameObject.GetComponent<MeshFilter>().sharedMesh;
        TrashMaterial = item.gameObject.GetComponent<MeshRenderer>().sharedMaterial;
        Scale = item.OriginalScale;

        TrashCamera = trashCamera;
        TrashCapturePoint = trashPoint;

        TrashItem = item;

        IsInitialized = true;
    }

    private void Update()
    {
        if(!IsInitialized)
        {
            return;
        }
        Rotation.eulerAngles += Vector3.up * 100 * Time.deltaTime;
        if (!IsDisposed)
        {
            DrawModel();
        }
        else
        {
            if(Vector3.Distance(transform.localPosition, DisposalPoint) <= 5f)
            {
                Destroy(gameObject);
                return;
            }
            transform.localPosition = Vector3.Lerp(transform.localPosition, DisposalPoint, 0.1f);
        }
    }

    private void DrawModel()
    {
        if(IsDisposed)
        {
            return;
        }
        RenderTexture.ReleaseTemporary(ImageTexture);
        ImageTexture = RenderTexture.GetTemporary(1024, 1024, 24);

        TrashCapturePoint.GetComponent<MeshFilter>().mesh = TrashMesh;
        TrashCapturePoint.GetComponent<MeshRenderer>().material = TrashMaterial;
        TrashCapturePoint.transform.rotation = Rotation;
        TrashCapturePoint.transform.localScale = Scale;
        TrashCamera.targetTexture = ImageTexture;
        TrashCamera.Render();

        ImageView.texture = ImageTexture;
    }

    public void Dispose()
    {
        IsDisposed = true;
    }

    private void OnDestroy()
    {
        try
        {
            Shared.GetMapManager().GetComponent<GameManager>().TextureReleasePool.Enqueue(ImageTexture);
        }
        catch
        {
            TrashCamera.targetTexture = null;
            RenderTexture.ReleaseTemporary(ImageTexture);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoubleScore : Powerup
{
    public override void DoPowerUp()
    {
        Manager.ScorePanel.SetScoreMultiplier(2);
        base.DoPowerUp();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Powerup : MonoBehaviour, IPowerup
{
    protected GameManager Manager;
    /// <summary>
    /// Call this base method at the end of your method.
    /// </summary>
    public virtual void DoPowerUp()
    {
        Destroy(gameObject);
        Shared.GameManager.NumPowerups++;
    }

    // Start is called before the first frame update
    protected virtual void Start()
    {
        Manager = Shared.GameManager;
        Invoke(nameof(SelfDestruct), 20);
    }

    protected virtual void Update()
    {
        if(Manager.PushdownStatus.Peek() != GameSatus.Pause)
        {
           transform.position += Vector3.down * 20 * Time.deltaTime;
        }
        transform.Rotate(Vector3.up * 100 * Time.deltaTime);
        if(Shared.GameManager.PushdownStatus.Peek() == GameSatus.Gameover)
        {
            SelfDestruct();
        }
    }

    private void SelfDestruct()
    {
        Destroy(gameObject);
        Shared.GameManager.NumPowerups++;
    }
}

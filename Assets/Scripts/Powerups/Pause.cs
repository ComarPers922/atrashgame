﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : Powerup
{
    public override void DoPowerUp()
    {
        Manager.PauseTrash(5);
        base.DoPowerUp();
    }
}

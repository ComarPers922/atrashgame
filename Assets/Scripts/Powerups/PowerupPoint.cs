﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupPoint : Spawn
{
    protected override void Start()
    {
        if(Manager.Powerups.Length <= 0)
        {
            return;
        }
        base.Start();
    }

    protected override void SpawnObject()
    {
        if(Manager.NumPowerups > 0 && Manager.PushdownStatus.Peek() == GameSatus.InGame)
        {
            Manager.NumPowerups--;
            var newPowerup = Instantiate(Manager.Powerups[Random.Range(0, Manager.Powerups.Length)]);
            newPowerup.transform.position = transform.position + new Vector3(Random.Range(-300, 300), 0, Random.Range(-100, 100));
            newPowerup.transform.rotation = Quaternion.identity;
        }
        base.SpawnObject();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeBonus : Powerup
{
    public override void DoPowerUp()
    {
        Shared.GameManager.GameTime.BonusTime(15);
        base.DoPowerUp();
    }
}

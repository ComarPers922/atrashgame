﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WheelAngle : MonoBehaviour
{
    [SerializeField]
    private WheelCollider WheelCollider;

    void Update()
    {
        Vector3 wheelPos;
        Quaternion wheelRot;
        WheelCollider.GetWorldPose(out wheelPos, out wheelRot);

        transform.rotation = wheelRot;
    }
}

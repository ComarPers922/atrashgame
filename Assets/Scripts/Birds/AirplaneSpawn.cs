﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirplaneSpawn : Spawn
{
    protected override void Start()
    {
        if(Manager.Airplanes.Length <= 0)
        {
            return;
        }
        base.Start();
    }

    protected override void SpawnObject()
    {
        if(Manager.NumAirplanes > 0)
        {
            Manager.NumAirplanes--;
            var newAirplane = Instantiate(Manager.Airplanes[Random.Range(0, Manager.Airplanes.Length)]);
            newAirplane.transform.position = transform.position + Random.Range(-20f, 20f) * Vector3.up;
            newAirplane.transform.rotation = transform.rotation;
            newAirplane.transform.Rotate(Vector3.up * Random.Range(-25, 25));
            newAirplane.transform.Rotate(Vector3.left * Random.Range(0f, 5f));
        }
        base.SpawnObject();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdSpawn : Spawn
{
    protected override void Start()
    {
        if(Manager.Birds.Length <= 0)
        {
            return;
        }
        base.Start();
    }

    protected override void SpawnObject()
    {
        if(Manager.NumBirds > 0)
        {
            Manager.NumBirds--;
            var newBird = Instantiate(Manager.Birds[Random.Range(0, Manager.Birds.Length)]);
            newBird.transform.position = transform.position + Random.Range(-50f, 50f) * Vector3.up;
            newBird.transform.rotation = transform.rotation;
            newBird.transform.Rotate(Vector3.up * Random.Range(-25, 25));
            newBird.transform.Rotate(Vector3.left * Random.Range(0f, 5f));
        }
        base.SpawnObject();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdBehavior : MonoBehaviour
{
    [SerializeField]
    private float MinSpeed = 300;
    [SerializeField]
    private float MaxSpeed = 470;

    private float Speed = 300;
    void Start()
    {
        Speed = Random.Range(MinSpeed, MaxSpeed);
        Invoke(nameof(FlyOffScreen), 20f);
    }

    void Update()
    {
        transform.Translate(Vector3.forward * Speed * Time.deltaTime);
    }

    private void FlyOffScreen()
    {
        Shared.GetMapManager().GetComponent<GameManager>().NumBirds++;
        Destroy(gameObject);
    }
}

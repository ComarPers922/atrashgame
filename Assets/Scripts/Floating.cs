﻿using UnityEngine;
using UnityStandardAssets.Water;

public class Floating : MonoBehaviour
{
    [SerializeField]
    private float DepthConstant = 0f;

    private static int InitRandSeed;

    private float SinOffset;
    private float RotationSpeed;

    private float VelocityAcc = 0;

    private AudioSource WaterAudio;

    private void Awake()
    {
        Random.InitState((int)Time.time + InitRandSeed);
        SinOffset = Random.Range(Mathf.PI / 2, Mathf.PI);
        RotationSpeed = Random.Range(3, 5f);
        InitRandSeed += (int)SinOffset;
        InvokeRepeating(nameof(IncreaseDifficulty), 30f, 30f);

        WaterAudio = GetComponent<AudioSource>();
    }

    private void IncreaseDifficulty()
    {
        if(Shared.GameManager.PushdownStatus.Peek() != GameSatus.InGame)
        {
            return;
        }
        VelocityAcc += 2.0f;
        VelocityAcc = Mathf.Min(12.5f, VelocityAcc);

        WaterAudio.clip = Shared.GameManager.SpeedUpSound;
        WaterAudio.Play();
    }

    private void OnTriggerEnter(Collider other)
    {
        var rigid = other.GetComponent<Rigidbody>();
        if (rigid == null)
        {
            rigid = other.GetComponentInParent<Rigidbody>();
        }
        if (rigid.velocity.magnitude >= 10)
        {
            rigid.isKinematic = true;
            rigid.velocity = Vector3.zero;
            rigid.angularVelocity = Vector3.zero;
            rigid.isKinematic = false;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        var otherRigid = other.GetComponent<Rigidbody>();
        if(otherRigid == null)
        {
            otherRigid = other.GetComponentInParent<Rigidbody>();
        }

        float maxDepthY = 0;
        float deltaY = DepthConstant - other.gameObject.transform.position.y;
        if (other.CompareTag("Trash") && !other.GetComponent<TrashBehavior>().IsCaught &&
            !other.GetComponent<TrashBehavior>().IsBlocked)
        {
            maxDepthY = -3;
            deltaY -= 2;
            if (!Shared.GameManager.IsTrashPaused && 
                Shared.GameManager.PushdownStatus.Peek() != GameSatus.Pause)
            {
                otherRigid.transform.position += Vector3.forward * VelocityAcc * Time.deltaTime;
                otherRigid.velocity += Vector3.forward * Random.Range(5f, 15f);
                otherRigid.angularVelocity = Vector3.Lerp(otherRigid.angularVelocity, Vector3.zero, 0.2f);
                otherRigid.AddForce(20000 * Vector3.forward);
                otherRigid.AddTorque(20000 * Vector3.up);
            }
            else
            {
                otherRigid.velocity = Vector3.zero;
                otherRigid.angularVelocity = Vector3.zero;
            }
        }
        otherRigid.transform.position = new Vector3(otherRigid.transform.position.x, 
            Mathf.Max(otherRigid.transform.position.y, -1.5f),
            otherRigid.transform.position.z);

        otherRigid.AddForce(otherRigid.mass * 2 * 
            Mathf.Max(maxDepthY, deltaY) * Vector3.up);
        otherRigid.transform.Rotate(Vector3.forward * Mathf.Sin(Time.time + SinOffset) * RotationSpeed * Time.deltaTime);
        if (otherRigid.velocity.magnitude >= 10)
        {
            otherRigid.velocity = otherRigid.velocity.normalized * 10;
        }
    }
}

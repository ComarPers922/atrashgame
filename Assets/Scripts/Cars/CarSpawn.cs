﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class CarSpawn : Spawn
{
    protected override void Start()
    {
        if(Manager.Cars.Length <= 0)
        {
            return;
        }
        base.Start();
    }

    protected override void SpawnObject()
    {
        if(Manager.NumCars > 0)
        {
            Manager.NumCars--;
            var newCar = Instantiate(Manager.Cars[Random.Range(0, Manager.Cars.Length)]);
            newCar.transform.position = transform.position + 10 * Vector3.up;
            newCar.transform.rotation = transform.rotation;
        }
        base.SpawnObject();
    }
}

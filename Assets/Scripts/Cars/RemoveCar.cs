﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveCar : MonoBehaviour
{
    private GameManager TransportationManager;
    private void Start()
    {
        TransportationManager = Shared.GetMapManager().GetComponent<GameManager>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Car"))
        {
            Destroy(other.transform.parent.gameObject);
            TransportationManager.NumCars++;
        }
    }
}

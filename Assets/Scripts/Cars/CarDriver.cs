﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarDriver : MonoBehaviour
{
    [SerializeField]
    private WheelCollider FL;
    [SerializeField]
    private WheelCollider FR;
    [SerializeField]
    private WheelCollider RL;
    [SerializeField]
    private WheelCollider RR;

    private Rigidbody Rigidbody;

    // Start is called before the first frame update
    void Start()
    {
        Rigidbody = GetComponent<Rigidbody>();
        Rigidbody.velocity = transform.forward * Random.Range(5f, 5.3f) * 10;
        RL.motorTorque = 50000f;
        RR.motorTorque = 50000f;

        FL.motorTorque = 10000f;
        FR.motorTorque = 10000f;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MobileCollect : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public void OnPointerDown(PointerEventData eventData)
    {
        CustomInput.SetAxis("CollectMobile", 1f);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        CustomInput.SetAxis("CollectMobile", 0f);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MobileRelease : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public void OnPointerDown(PointerEventData eventData)
    {
        CustomInput.SetAxis("Release", 1f);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        CustomInput.SetAxis("Release", 0f);
    }
}

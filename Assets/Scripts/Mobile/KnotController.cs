﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class KnotController : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    [SerializeField]
    private float DeadZoneX = 100;
    [SerializeField]
    private float DeadZoneY = 100;
    // Start is called before the first frame update

    private Vector2 StartPosition;

    public void OnBeginDrag(PointerEventData eventData)
    {
        StartPosition = eventData.pointerCurrentRaycast.screenPosition;
        PreviousPointerPosition = eventData.position;
    }

    private Vector2 PreviousPointerPosition;
    public void OnDrag(PointerEventData eventData)
    {   
        if(Vector2.Distance(PreviousPointerPosition, eventData.position) > 500)
        {
            transform.localPosition = Vector3.zero;
            CustomInput.SetAxis("Horizontal", 0);
            CustomInput.SetAxis("Vertical", 0);
            return;
        }
        PreviousPointerPosition = eventData.position;
        var deltaVec2 = eventData.position - StartPosition;
        deltaVec2 = new Vector2(Mathf.Clamp(deltaVec2.x, -DeadZoneX, DeadZoneX),
                                                Mathf.Clamp(deltaVec2.y, -DeadZoneY, DeadZoneY));
        transform.localPosition = deltaVec2 * 0.8f;
        if(Mathf.Abs(deltaVec2.x / DeadZoneX) < 0.5f)
        {
            deltaVec2.x = 0;
        }
        CustomInput.SetAxis("Horizontal", deltaVec2.x / DeadZoneX);
        CustomInput.SetAxis("Vertical", deltaVec2.y / DeadZoneY);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        transform.localPosition = Vector3.zero;
        CustomInput.SetAxis("Horizontal", 0);
        CustomInput.SetAxis("Vertical", 0);
    }
}

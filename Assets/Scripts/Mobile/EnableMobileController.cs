﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableMobileController : MonoBehaviour
{
    [SerializeField]
    private GameObject[] UIsToBeDisabled;
    void Start()
    {
#if !UNITY_ANDROID && !UNITY_IOS
        foreach (var item in UIsToBeDisabled)
        {
            item.SetActive(false);
        }
#endif
        gameObject.SetActive(false);
    }
}

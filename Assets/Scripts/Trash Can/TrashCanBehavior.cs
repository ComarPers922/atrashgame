﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;
public class TrashCanBehavior : MonoBehaviour
{
    [SerializeField]
    private TrashType TrashcanType;
    [SerializeField]
    private TrashcanBubble Bubble;

    private readonly Queue<TrashBehavior> Trashes = new Queue<TrashBehavior>();

    private Animator Animator;
    private ScorePanel Score;

    private const float ProcessingTime = 1;

    private Vibration Vibration;

    private AudioSource TrashcanAudio;

    private void Start()
    {
        Animator = GetComponent<Animator>();
        Score = Shared.GetMapManager().GetComponent<GameManager>().ScorePanel;
        Vibration = Shared.GameManager.VibrationManager;
        TrashcanAudio = GetComponent<AudioSource>();
    }

    public void ThrowTrash(TrashBehavior trash)
    {
        trash.transform.position = transform.position + Vector3.up * 50;
        trash.gameObject.SetActive(true);
        trash.GetComponent<Rigidbody>().velocity = Vector3.up * -50;
        trash.GetComponent<Rigidbody>().AddForce(Vector3.up * -1, ForceMode.VelocityChange);
    }

    private void OnTriggerEnter(Collider other)
    {
        var trashBehavior = other.gameObject.GetComponent<TrashBehavior>();
        if(trashBehavior != null)
        {
            Animator.SetTrigger("Eat");
            TrashcanAudio.clip = Shared.GameManager.TrashProcessing;
            TrashcanAudio.Play();
            if (Trashes.Count <= 0)
            {
                Invoke(nameof(ProcessTrash), ProcessingTime);
            }
            Trashes.Enqueue(trashBehavior);
            trashBehavior.gameObject.SetActive(false);
        }
    }

    private void ProcessTrash()
    {
        var trash = Trashes.Dequeue();
        if(trash.GetTrashType() == TrashcanType)
        {
            // Vibration.ShortVibrate(2);
            Bubble.ShowConversation($"{Localization.GetLocalizedText("yummy")}");
            Score.AddGoodCount();
            Score.AddScore();
            TrashcanAudio.clip = Shared.GameManager.TrashCorrect;
            TrashcanAudio.Play();
            Destroy(trash.gameObject);
        }
        else
        {
            Bubble.ShowConversation($"{Localization.GetLocalizedText("itis")}{LocalizedTrashLabel.TranslateTrashType(trash.GetTrashType())}:(!");
            Vibration.LongVibrate(1);
            Animator.SetTrigger("Pop");
            TrashcanAudio.clip = Shared.GameManager.TrashWrong;
            TrashcanAudio.Play();
            Score.AddBadCount();
            Score.MinusScoreWrongCollection();
            trash.transform.position = transform.position + transform.forward * 100 + Vector3.up * 10;
            trash.gameObject.SetActive(true);
        }
        if (Trashes.Count > 0)
        {
            Invoke(nameof(ProcessTrash), ProcessingTime);
        }
    }
}

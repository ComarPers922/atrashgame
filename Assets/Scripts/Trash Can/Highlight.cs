﻿using UnityEngine;
using UnityEngine.UI;

public class Highlight : MonoBehaviour
{
    [SerializeField]
    private GameObject TrashCan;
    [SerializeField]
    private GameObject TipText;

    private Renderer[] Renderers;

    private Animator Animator;

    private Image TipTextBackground;

    private Color OriginalColor;

    private Color TargetColor;

    [SerializeField]
    private GameObject Player;

    private bool IsSelected = false;

    private Vector3 OriginalTextPosition;

    private void Start()
    {
        if(TrashCan == null)
        {
            return;
        }
        Animator = TrashCan.GetComponent<Animator>();
        Renderers = new Renderer[TrashCan.transform.childCount];
        for (int i = 0; i < Renderers.Length; i++)
        {
            Renderers[i] = TrashCan.transform.GetChild(i).GetComponent<Renderer>();
        }
        TipTextBackground = TipText.GetComponent<Image>();
        OriginalColor = TipTextBackground.color;
        TargetColor = OriginalColor;
        OriginalTextPosition = TipText.transform.position;
    }
    private float T = 0;
    private int Increment = 1;
    private void Update()
    {
        if(T >= 1)
        {
            Increment = -1;
        }
        else if(T <= 0)
        {
            Increment = 1;
        }
        T += (IsSelected ? 0.1f : 0.01f) * Increment;
        TipText.transform.position = Vector3.Lerp(OriginalTextPosition, OriginalTextPosition + Vector3.up * 7, T);
        TipTextBackground.color = Color.Lerp(TipTextBackground.color, TargetColor, 0.05f);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Boat"))
        {
            BoatDriver driver = other.GetComponent<BoatDriver>();
            if (driver != null)
            {
                driver.TargetTrashCan = TrashCan.GetComponent<TrashCanBehavior>();
                IsSelected = true;
                TargetColor = new Color(1, .7f, .7f, OriginalColor.a);
                Animator.speed = 2;
                foreach (var item in Renderers)
                {
                    item.material.SetFloat("_IsHighlighted", 1f);
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        BoatDriver driver = other.GetComponent<BoatDriver>();
        if (driver != null)
        {
            if (driver.TargetTrashCan == TrashCan.GetComponent<TrashCanBehavior>())
            {
                driver.TargetTrashCan = null;
            }
            TargetColor = OriginalColor;
            IsSelected = false;
            Animator.speed = 1;
            foreach (var item in Renderers)
            {
                item.material.SetFloat("_IsHighlighted", 0f);
            }
        }
    }
}

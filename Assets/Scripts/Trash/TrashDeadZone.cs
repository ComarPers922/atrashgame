﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrashDeadZone : MonoBehaviour
{
    private GameManager Manager;

    private AudioSource DeadZoneAudio;
    private void Start()
    {
        Manager = Shared.GetMapManager().GetComponent<GameManager>();
        DeadZoneAudio = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Trash"))
        {
            DeadZoneAudio.clip = Manager.TrashFled;
            DeadZoneAudio.Play();
            Destroy(other.gameObject);
            Manager.NumTrashes++;
            Manager.ScorePanel.AddMissCount();
            Manager.ScorePanel.MinusScoreMiss();
        }
    }
}

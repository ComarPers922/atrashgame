﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEditor;
using UnityEngine;

public class TrashBehavior : MonoBehaviour
{
    [SerializeField]
    private TrashType TrashType;
    public TrashType GetTrashType()
    {
        return TrashType;    
    }

    public Vector3 OriginalScale { private set; get; }

    private Vector3 OriginalPosition;
    private Quaternion OriginalRotation;
    private bool IsCorrecting = true;
    private Rigidbody Rigidbody;

    public bool IsCaught { private set; get; }
    public BoatDriver TrashCollector { private set; get; }

    private GameManager GameManager;

    private AudioSource HazardousSound;

    private float HazardousPenaltyTime = 5f;

    private const float PENALTY_TIME_DELTA = 0.3f;
    void Start()
    {
        OriginalPosition = transform.position;
        OriginalRotation = transform.rotation;
        OriginalScale = transform.localScale;
        Rigidbody = GetComponent<Rigidbody>();
        GameManager = Shared.GetMapManager().GetComponent<GameManager>();
        if (TrashType == TrashType.Hazardous)
        {
            HazardousSound = gameObject.AddComponent<AudioSource>();
            HazardousSound.loop = false;
            HazardousSound.clip = GameManager.HazardousTrashEmerged;
            HazardousSound.Play();
            Invoke(nameof(Penalize), (HazardousPenaltyTime -= PENALTY_TIME_DELTA) * 4.7f);
        }
    }

    /// <summary>
    /// Hazardous trashes are bring penalities to players.
    /// </summary>
    private void Penalize()
    {
        if(TrashType != TrashType.Hazardous)
        {
            return;
        }
        if(IsCaught)
        {
            return;
        }
        GameManager.ScorePanel.MinusScoreHazardous();
        HazardousSound.clip = GameManager.HazardousTrash;
        HazardousSound.Play();
        Invoke(nameof(Penalize), Mathf.Max(HazardousPenaltyTime -= PENALTY_TIME_DELTA, 3));
    }

    public void StartPenalty()
    {
        if(TrashType != TrashType.Hazardous)
        {
            return;
        }
        Invoke(nameof(Penalize), Mathf.Max(HazardousPenaltyTime, 3)); 
    }

    private void Update()
    {
        if(GameManager.PushdownStatus.Peek() != GameSatus.InGame && 
            GameManager.PushdownStatus.Peek() != GameSatus.TutorialGame &&
            GameManager.PushdownStatus.Peek() != GameSatus.Pause)
        {
            Destroy(gameObject);
            return;
        }
    }

    void FixedUpdate()
    {
        if(IsCaught && Shared.GameManager.PushdownStatus.Peek() != GameSatus.Pause)
        {
            transform.localScale = Vector3.Lerp(OriginalScale, 
                OriginalScale / 2, 
                Vector3.Distance(transform.position, 
                    TrashCollector.transform.position));
            Rigidbody.AddForce((TrashCollector.transform.position - (transform.position + Vector3.up * 3)).normalized * Rigidbody.mass * 300, ForceMode.Impulse);
            return;
        }
        transform.localScale = Vector3.Lerp(transform.localScale, OriginalScale, 0.1f);
        Rigidbody.AddForce(Vector3.up * -Rigidbody.mass * 10);
        if (IsCorrecting)
        {
            if((OriginalPosition - transform.position).magnitude >= 2f)
            {
                transform.position = Vector3.Lerp(transform.position, 
                    Vector3.up * OriginalPosition.y + 
                    Vector3.right * transform.position.x + 
                    Vector3.forward * transform.position.z, 
                    0.1f);
            }
            if((OriginalRotation.eulerAngles - transform.rotation.eulerAngles).magnitude >= 2f)
            {
                transform.rotation = Quaternion.Lerp(transform.rotation, OriginalRotation, 0.1f);
            }
        }
    }

    public bool IsBlocked { private set; get; }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.CompareTag("Boat"))
        {
            if(IsCorrecting)
            {
                Invoke(nameof(Refresh), 2f);
                IsCorrecting = false;
            }
        }
    }
    public void Block()
    {
        IsBlocked = true;
    }
    private void OnDestroy()
    {
        CancelInvoke();
        GameManager.NumTrashes = Math.Min(GameManager.NumTrashes + 1, 25);
    }

    private void Refresh()
    {
        IsCorrecting = true;
    }

    public void IsCaughtBy(BoatDriver collector)
    {
        CancelInvoke(nameof(Penalize));
        TrashCollector = collector;
        IsCaught = true;
    }

    public void IsReleasedBy(BoatDriver reseaser)
    {
        TrashCollector = null;
        IsCaught = false;
        if (TrashType == TrashType.Hazardous)
        {
            Invoke(nameof(Penalize), Mathf.Max(HazardousPenaltyTime, 3));
        }
    }
}

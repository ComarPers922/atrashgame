﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design;
using UnityEngine;

public class TrashSpawn : Spawn
{
    private GameManager GameManager;
    protected override void Awake()
    {
        UseStartIncrement = true;
        base.Awake();
    }
    protected override void Start()
    {
        GameManager = Shared.GetMapManager().GetComponent<GameManager>();
        if(Manager.Trashes.Length <= 0)
        {
            return;
        }
        base.Start();
    }

    protected override void SpawnObject()
    {
        if(!gameObject.activeSelf)
        {
            Invoke(nameof(SpawnObject), 2f);
            return;
        }
        if(Manager.NumTrashes > 0 && (GameManager.PushdownStatus.Peek() == GameSatus.InGame
            || (GameManager.PushdownStatus.Peek() == GameSatus.TutorialGame && 
                    Shared.TutorialGameManager.IsReleasingTrash)))
        {
            Manager.NumTrashes--;
            GameObject newTrash = null;
            if (GameManager.PushdownStatus.Peek() == GameSatus.TutorialGame)
            {
                newTrash = Instantiate(Shared.TutorialGameManager.PaperTrashes[Random.Range(0, Shared.TutorialGameManager.PaperTrashes.Length)]);
            }
            else
            {
                newTrash = Instantiate(Manager.Trashes[Random.Range(0, Manager.Trashes.Length)]);
            }
            newTrash.GetComponent<Rigidbody>().velocity = Vector3.forward * Random.Range(50f, 100f);
            newTrash.transform.position = transform.position + Random.Range(-10f, 10f) * Vector3.left;
            newTrash.transform.rotation = transform.rotation;
            newTrash.transform.Rotate(Vector3.up * Random.Range(-90, 90));
            newTrash.transform.Rotate(Vector3.left * Random.Range(25, 30));
        }
        if (GameManager.PushdownStatus.Peek() == GameSatus.InGame
            || (GameManager.PushdownStatus.Peek() == GameSatus.TutorialGame &&
                    Shared.TutorialGameManager.IsReleasingTrash))
        {
            base.SpawnObject();
        }
        else
        {
            Invoke(nameof(SpawnObject), 2f);
            return;
        }
    }
}

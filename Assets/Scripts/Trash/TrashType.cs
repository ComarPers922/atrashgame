﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TrashType
{
    Plastic, Glass, Paper, Metal, Hazardous, General
}

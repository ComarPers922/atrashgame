﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class ThirdPersonCamera : MonoBehaviour
{
    [SerializeField]
    private GameObject Target;

    [SerializeField]
    private float Distance = 100;

    [SerializeField]
    private float Height = 50;

    private float distance;

    private AudioSource BGM;

    private void Start()
    {
        BGM = GetComponent<AudioSource>();
    }
    private void FixedUpdate()
    {
        float Vertical = CustomInput.GetAxis("Vertical");
        if(Vertical > 0.1f)
        {
            distance = Distance - 20;
        }
        else if(Vertical < 0.1f)
        {
            distance = Distance + 20;
        }
        else
        {
            distance = Distance;
        }
    }
    private bool wasPaused = false;
    void Update()
    {
        var dest = Target.transform.position +
                    //Target.transform.forward * -distance +
                    Vector3.forward * distance +
                    Vector3.up * Height;
        transform.position = Vector3.Lerp(transform.position, dest, 0.1f);
        if(Shared.GameManager.PushdownStatus.Peek() == GameSatus.Pause)
        {
            BGM.Pause();
            wasPaused = true;
        }
        else if(wasPaused)
        {
            BGM.Play();
            wasPaused = false;
        }
        if(Shared.GameManager.GameTime.GetCurrentTime() == 30)
        {
            BGM.Stop();
            BGM.Play();
        }
        if(Shared.GameManager.GameTime.GetCurrentTime() <= 30 &&
            (Shared.GameManager.PushdownStatus.Peek() == GameSatus.InGame ||
            Shared.GameManager.PushdownStatus.Peek() == GameSatus.TutorialGame))
        {
            BGM.pitch = 1.5f;
        }
        else
        {
            BGM.pitch = 1;
        }
        //var targetRotation = Quaternion.LookRotation(Target.transform.position - transform.position);
        //transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, 0.08f);
    }
}

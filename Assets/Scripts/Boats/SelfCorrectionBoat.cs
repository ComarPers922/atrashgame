﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class SelfCorrectionBoat : MonoBehaviour
{
    private Rigidbody Rigidbody;

    private void Start()
    {
        Rigidbody = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        transform.Translate(Vector3.up * -10 * Time.deltaTime);
        Rigidbody.AddForce(Vector3.up * -Rigidbody.mass * 10);
        if (Mathf.Abs(transform.rotation.eulerAngles.z) >= 2f ||
            Mathf.Abs(transform.rotation.eulerAngles.x) >= 1f)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation,
                Quaternion.Euler(new Vector3(0, transform.rotation.eulerAngles.y, 0)),
                0.3f);
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoatDriver : MonoBehaviour
{
    [SerializeField]
    private float NormalSpeed = 100;
    [SerializeField]
    private float SprintSpeed = 100;
    [SerializeField]
    private GameObject TrashArea;

    private Rigidbody Rigidbody;
    private bool IsSprinting = false;
    private GameManager GameManager;

    private HashSet<GameObject> CaughtTrashes = new HashSet<GameObject>();

    private bool IsFireKeyUp = true;

    private AudioSource[] BoatSounds;
    private AudioSource VacuumSound;
    private AudioSource OtherSound;

    private bool IsCollecting = false;

    public TrashCanBehavior TargetTrashCan { set; get; }

    void Start()
    {
        Rigidbody = GetComponent<Rigidbody>();
        GameManager = Shared.GetMapManager().GetComponent<GameManager>();
        BoatSounds = GetComponents<AudioSource>();

        VacuumSound = BoatSounds[0];
        OtherSound = BoatSounds[1];
    }

    private void Update()
    {
        TrashArea.transform.rotation = Quaternion.Euler(0, 0, 0);
    }

    void FixedUpdate()
    {
        float Vertical = -CustomInput.GetAxis("Vertical");
        float Horizontal = -CustomInput.GetAxis("Horizontal");
        TrashArea.SetActive(CustomInput.GetAxis("Collect") > 0.1f || 
            CustomInput.GetAxis("CollectMobile") > 0.1f);
        if((GameManager.PushdownStatus.Peek() == GameSatus.InGame ||
            GameManager.PushdownStatus.Peek() == GameSatus.TutorialGame) && 
            (CustomInput.GetAxis("Collect") > 0.1f || 
            CustomInput.GetAxis("CollectMobile") > 0.1f))
        {
            if(VacuumSound.clip != GameManager.VacuumRunning)
            {
                VacuumSound.clip = GameManager.VacuumRunning;
                VacuumSound.loop = true;
            }
            if (!VacuumSound.isPlaying)
            {
                VacuumSound.Play();
            }
            IsCollecting = true;
        }
        else
        {
            if (IsCollecting)
            {
                VacuumSound.Stop();
                VacuumSound.loop = false;
                VacuumSound.clip = GameManager.VacuumShutdown;
                VacuumSound.Play();
                IsCollecting = false;
            }
        }
        if (TargetTrashCan != null && CustomInput.GetAxis("Release") >= 0.1f && IsFireKeyUp)
        {
            var trash = GameManager.TrashViewer.Dequeue();
            if (trash != null)
            {
                trash.IsReleasedBy(this);
                TargetTrashCan.ThrowTrash(trash);
                OtherSound.clip = GameManager.TrashReleased;
                OtherSound.Play();
            }
            IsFireKeyUp = false;
        }
        if (CustomInput.GetAxis("Release") < 0.1f)
        {
            IsFireKeyUp = true;
        }
        if (!TrashArea.activeSelf)
        {
            foreach (var item in CaughtTrashes)
            {
                item.GetComponent<TrashBehavior>().IsReleasedBy(this);
            }
        }
        CaughtTrashes.Clear();
#if UNITY_ANDROID || UNITY_IOS
        IsSprinting = true;
#endif
        if (CustomInput.GetAxis("Sprint") > 0.001f)
        {
            IsSprinting = true;
        }
        var forward = new Vector3(Horizontal, 0, Vertical);
        var speed = NormalSpeed + (IsSprinting ? SprintSpeed : 0);
        speed *= Mathf.Max(0.4f,
            Mathf.Pow(1 - Shared.GameManager.TrashViewer.Count / (float)Shared.GameManager.TrashViewer.Capacity, 1.3f));
        if (forward.magnitude <= 0.001f)
        {
            IsSprinting = false;
            Rigidbody.angularVelocity = Vector3.zero;
            Rigidbody.velocity = Vector3.Lerp(Rigidbody.velocity, Vector3.zero, 0.1f);
            return;
        }
        var lookAtDir = Quaternion.LookRotation(forward.normalized, Vector3.up);

        var deltaX = (lookAtDir * Vector3.forward - transform.rotation * Vector3.forward).x;
        var deltaZ = (lookAtDir * Vector3.forward - transform.rotation * Vector3.forward).z;
        transform.rotation = Quaternion.Lerp(transform.rotation, lookAtDir, 0.4f);
        transform.rotation = Quaternion.Lerp(transform.rotation,
            Quaternion.Euler(transform.rotation.eulerAngles.x - (IsSprinting ? 30 : 20),
                transform.rotation.eulerAngles.y,
                Mathf.Clamp(deltaX, -1, 1) * Mathf.Clamp(deltaZ, -1, 1) * 100),
            0.1f);


        Rigidbody.velocity = transform.forward * Mathf.Lerp(0, speed, forward.magnitude);
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Trash"))
        {
            if (!CaughtTrashes.Contains(other.gameObject))
            {
                other.GetComponent<TrashBehavior>().IsCaughtBy(this);
                CaughtTrashes.Add(other.gameObject);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        other.GetComponent<TrashBehavior>()?.IsReleasedBy(this);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Trash") && TrashArea.activeSelf)
        {
            GameManager.TrashViewer.Enqueue(collision.collider.GetComponent<TrashBehavior>());
        }
        if(collision.collider.CompareTag("Powerup"))
        {
            OtherSound.clip = GameManager.Powerup;
            OtherSound.Play();
            var powerup = collision.collider.GetComponent<Powerup>();
            if(powerup == null)
            {
                powerup = collision.collider.GetComponentInParent<Powerup>();
            }
            powerup?.DoPowerUp();
        }
    }
}
